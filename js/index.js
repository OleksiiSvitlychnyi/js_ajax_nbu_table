
const url = "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json";

const request = new XMLHttpRequest();

const button = document.getElementById("btn");

button.addEventListener("click", () => {
    
    request.open("GET", url);
    request.send();
    request.addEventListener("readystatechange", () => {
        if(request.readyState === 4 && request.status >= 200 && request.status < 300) {
             showTable(JSON.parse(request.responseText));
        } else if(request.readyState === 4) {
            throw new Error("Error");
        }
    })
});

const showTable = (arr) => {

    const tbody = document.querySelector("tbody");

    tbody.innerHTML = "";

    const table = arr.map((obj, i) => {
        return `<tr><td>${i + 1}</td><td>${obj.txt}</td><td>${obj.rate}</td></tr>`
    }).join("");

    tbody.insertAdjacentHTML("beforeend", table)

}



